# ai-mode

ai-mode for Emacs


## add to .emacs config file:
```elisp
(add-to-list 'load-path "~/.emacs.d/ai-mode")
(require 'ai-mode)
```
## start:

M-x ai-mode

## using API:

api.tecorigin.com/v1/chat/completions

## AI Model:

deepseek-ai/DeepSeek-R1-Distill-Qwen-32B

## License

MIT
